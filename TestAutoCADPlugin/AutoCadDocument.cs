﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using TestAutoCADPlugin.Adapters;

namespace TestAutoCADPlugin
{
    public class AutoCadDocument : IDocument
    {
        private readonly Document document;
        private readonly Database db;

        // used to store state of entities
        private readonly List<IAutoCadEntity> processedEntities = new List<IAutoCadEntity>();

        public event EventHandler OnClose;

        public AutoCadDocument(Document document)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }
            this.document = document;
            db = document.Database;
            document.BeginDocumentClose += Document_BeginDocumentClose;
        }

        private void Document_BeginDocumentClose(object sender, DocumentBeginCloseEventArgs e)
        {
            if (OnClose != null)
            {
                OnClose(this, new EventArgs());
            }
        }

        public IEnumerable<ILayer> GetLayers()
        {
            processedEntities.Clear();
            using (var transaction = db.TransactionManager.StartOpenCloseTransaction())
            {
                var layers = LoadLayers(transaction);
                var primitives = LoadPrimitives(transaction);
                transaction.Commit();

                FillLayersByPrimitives(layers, primitives);

                AddProcessedEntities(layers);
                AddProcessedEntities(primitives);

                return layers;
            }   
        }

        private IEnumerable<LayerAdapter> LoadLayers(Transaction transaction)
        {
            var layers = new List<LayerTableRecord>();
            var layersTable = (LayerTable)transaction.GetObject(db.LayerTableId, OpenMode.ForRead);
            foreach (ObjectId layerId in layersTable)
            {
                var layer = (LayerTableRecord)transaction.GetObject(layerId, OpenMode.ForRead);
                layers.Add(layer);
            }
            return layers.Select(l => new LayerAdapter(l) { IsZero = (l.Id == db.LayerZero) }).ToList();
        }

        private IEnumerable<PrimitiveAdapter> LoadPrimitives(Transaction transaction)
        {
            var primitiveAdapters = new List<PrimitiveAdapter>();
            var btr = (BlockTableRecord)transaction.GetObject(SymbolUtilityServices.GetBlockModelSpaceId(db), OpenMode.ForRead);
            foreach (var objectId in btr)
            {
                var entity = (Entity)transaction.GetObject(objectId, OpenMode.ForRead);
                var primitiveAdapter = PrimitiveAdapter.ByEntity(entity);
                if (primitiveAdapter != null)
                {
                    primitiveAdapters.Add(primitiveAdapter);
                }
            }
            return primitiveAdapters;
        }

        private void FillLayersByPrimitives(IEnumerable<LayerAdapter> layers, IEnumerable<PrimitiveAdapter> primitives)
        {
            foreach (var layer in layers)
            {
                var primitivesOfLayer = primitives.Where(p => p.LayerId == layer.Id);
                layer.Primitives.AddRange(primitivesOfLayer);
            }
        }

        private void AddProcessedEntities(IEnumerable<IAutoCadEntity> entities)
        {
            var alreadyAddedEntities = entities.Where(e => processedEntities.Any(pe => pe.Id == e.Id)).ToList();
            processedEntities.AddRange(entities.Except(alreadyAddedEntities));
        }

        public void SaveChanges()
        {
            using (document.LockDocument()) // lock document to avoid "eLockViolation"
            {
                using (var transaction = db.TransactionManager.StartTransaction())
                {
                    foreach (var entity in processedEntities)
                    {
                        entity.AcceptChanges(transaction);
                    }
                    transaction.Commit();

                    // refresh graphical objects 
                    document.Editor.Regen();
                }
            }            
        }
    }
}
