﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TestAutoCADPlugin
{
    public interface ILayer
    {
        Color Color { get; set; }

        string Name { get; set; }

        bool IsVisible { get; set; }

        bool IsZero { get; set; }

        List<IPrimitive> Primitives { get; set; }
    }
}
