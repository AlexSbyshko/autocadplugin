﻿using System;
using Autodesk.AutoCAD.ApplicationServices.Core;

namespace TestAutoCADPlugin
{
    public class AutoCad : IDocumentManager
    {
        public event EventHandler CurrentDocumentChanged;

        public AutoCad()
        {
            Application.DocumentManager.DocumentBecameCurrent += Documents_DocumentBecameCurrent;
        }

        private void Documents_DocumentBecameCurrent(object sender, Autodesk.AutoCAD.ApplicationServices.DocumentCollectionEventArgs e)
        {
            if (CurrentDocumentChanged != null)
            {
                CurrentDocumentChanged(this, new EventArgs());
            }
        }

        public IDocument CurrentDocument
        {
            get
            {
                if (Application.DocumentManager.MdiActiveDocument == null)
                {
                    return null;
                }
                return new AutoCadDocument(Application.DocumentManager.MdiActiveDocument);                
            }
        }        
    }
}
