﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin
{
    public interface IPoint : IPrimitive
    {
        IPosition Position { get; set; }
    }
}
