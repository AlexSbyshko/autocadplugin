﻿namespace TestAutoCADPlugin.ViewModels
{
    public class LineViewModel : PrimitiveViewModel
    {
        private ILine line;

        public LineViewModel(ILine line)
            : base(line)
        {
            this.line = line;
        }

        public double StartX
        {
            get
            {
                return line.StartPosition.X;
            }
            set
            {
                line.StartPosition.X = value;
                OnPropertyChanged();
            }
        }

        public double StartY
        {
            get
            {
                return line.StartPosition.Y;
            }
            set
            {
                line.StartPosition.Y = value;
                OnPropertyChanged();
            }
        }

        public double StartZ
        {
            get
            {
                return line.StartPosition.Z;
            }
            set
            {
                line.StartPosition.Z = value;
                OnPropertyChanged();
            }
        }

        public double EndX
        {
            get
            {
                return line.EndPosition.X;
            }
            set
            {
                line.EndPosition.X = value;
                OnPropertyChanged();
            }
        }

        public double EndY
        {
            get
            {
                return line.EndPosition.Y;
            }
            set
            {
                line.EndPosition.Y = value;
                OnPropertyChanged();
            }
        }

        public double EndZ
        {
            get
            {
                return line.EndPosition.Z;
            }
            set
            {
                line.EndPosition.Z = value;
                OnPropertyChanged();
            }
        }
    }
}
