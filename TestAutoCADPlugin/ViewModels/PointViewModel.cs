﻿namespace TestAutoCADPlugin.ViewModels
{
    public class PointViewModel : PrimitiveViewModel
    {
        private IPoint point;

        public PointViewModel(IPoint point)
            : base(point)
        {
            this.point = point;
        }

        public double X
        {
            get
            {
                return point.Position.X;
            }
            set
            {
                point.Position.X = value;
                OnPropertyChanged();
            }
        }

        public double Y
        {
            get
            {
                return point.Position.Y;
            }
            set
            {
                point.Position.Y = value;
                OnPropertyChanged();
            }
        }

        public double Z
        {
            get
            {
                return point.Position.Z;
            }
            set
            {
                point.Position.Z = value;
                OnPropertyChanged();
            }
        }
    }
}
