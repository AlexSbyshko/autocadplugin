﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Messaging;
using System.Linq;
using GalaSoft.MvvmLight.Command;

namespace TestAutoCADPlugin.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private IDocumentManager documentManager;
        private IDocument document;

        public RelayCommand LoadLayersCommand { get;  set; }
        public RelayCommand SaveChangesCommand { get; set; }

        /// <summary>
        /// Constructor for testing purpose
        /// </summary>
        /// <param name="documentManager"></param>
        public MainViewModel(IDocumentManager documentManager)
            : this()
        {
            ReceiveDocument(documentManager);
        }

        public MainViewModel()
        {
            LoadLayersCommand = new RelayCommand(LoadLayers);
            SaveChangesCommand = new RelayCommand(SaveChanges);
            Messenger.Default.Register<IDocumentManager>(this, ReceiveDocument);
            Messenger.Default.Register<Message>(this, ReceiveMessage);
        }

        private void ReceiveDocument(IDocumentManager documentManager)
        {
            if (documentManager != null)
            {
                this.documentManager = documentManager;
                documentManager.CurrentDocumentChanged += DocumentManager_CurrentDocumentChanged;
                UpdateDocument();
            }            
        }

        private void DocumentManager_CurrentDocumentChanged(object sender, System.EventArgs e)
        {
            UpdateDocument();
        }

        private void UpdateDocument()
        {
            document = documentManager?.CurrentDocument;
            if (document != null)
            {
                document.OnClose += Document_OnClose;
                LoadLayers();
            }
        }

        private void Document_OnClose(object sender, System.EventArgs e)
        {
            ClearLayers();
        }

        private void ReceiveMessage(Message message)
        {
            if (message == Message.LoadLayers)
            {
                LoadLayers();
            }
        }

        private void LoadLayers()
        {
            ClearLayers();

            if (document == null)
            {
                return;
            }

            foreach (var layer in document.GetLayers())
            {
                var layerViewModel = new LayerViewModel(layer);
                Layers.Add(layerViewModel);

                if (layer.Primitives == null)
                {
                    continue;
                }

                foreach (var point in layer.Primitives.OfType<IPoint>())
                {
                    layerViewModel.Points.Add(new PointViewModel(point));
                }
                foreach (var line in layer.Primitives.OfType<ILine>())
                {
                    layerViewModel.Lines.Add(new LineViewModel(line));
                }
                foreach (var circle in layer.Primitives.OfType<ICircle>())
                {
                    layerViewModel.Circles.Add(new CircleViewModel(circle));
                }
            }
        }

        private void ClearLayers()
        {
            foreach (var layer in Layers.ToList())
            {
                Layers.Remove(layer);
            }
        }

        private void SaveChanges()
        {
            if (document != null)
            {
                document.SaveChanges();
            }
        }

        public ObservableCollection<LayerViewModel> Layers { get; set; } = new ObservableCollection<LayerViewModel>();
    }
}
