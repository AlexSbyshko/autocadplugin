﻿using System.Collections.ObjectModel;
using System.Windows.Media;

namespace TestAutoCADPlugin.ViewModels
{
    public class LayerViewModel : BaseViewModel
    {
        private ILayer layer;

        public LayerViewModel(ILayer layer)
        {
            this.layer = layer;
        }

        public ObservableCollection<PointViewModel> Points { get; set; } = new ObservableCollection<PointViewModel>();
        public ObservableCollection<LineViewModel> Lines { get; set; } = new ObservableCollection<LineViewModel>();
        public ObservableCollection<CircleViewModel> Circles { get; set; } = new ObservableCollection<CircleViewModel>();

        public Color Color
        {
            get
            {
                return layer.Color;
            }
            set
            {
                layer.Color = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                return layer.Name;
            }
            set
            {
                layer.Name = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisible
        {
            get
            {
                return layer.IsVisible;
            }
            set
            {
                layer.IsVisible = value;
                OnPropertyChanged();
            }
        }

        public bool IsZero
        {
            get
            {
                return layer.IsZero;
            }
        }
    }
}
