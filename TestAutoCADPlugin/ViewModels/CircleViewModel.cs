﻿namespace TestAutoCADPlugin.ViewModels
{
    public class CircleViewModel : PrimitiveViewModel
    {
        private ICircle circle;
        public CircleViewModel(ICircle circle)
            : base(circle)
        {
            this.circle = circle;
        }

        public double CenterX
        {
            get
            {
                return circle.CenterPosition.X;
            }
            set
            {
                circle.CenterPosition.X = value;
                OnPropertyChanged();
            }
        }

        public double CenterY
        {
            get
            {
                return circle.CenterPosition.Y;
            }
            set
            {
                circle.CenterPosition.Y = value;
                OnPropertyChanged();
            }
        }

        public double CenterZ
        {
            get
            {
                return circle.CenterPosition.Z;
            }
            set
            {
                circle.CenterPosition.Z = value;
                OnPropertyChanged();
            }
        }

        public double Radius
        {
            get
            {
                return circle.Radius;
            }
            set
            {
                circle.Radius = value;
                OnPropertyChanged();
            }
        }
    }
}
