﻿namespace TestAutoCADPlugin.ViewModels
{
    public class PrimitiveViewModel : BaseViewModel
    {
        private IPrimitive primitive;

        public PrimitiveViewModel(IPrimitive primitive)
        {
            this.primitive = primitive;
        }

        public double Thickness
        {
            get
            {
                return primitive.Thickness;
            }
            set
            {
                primitive.Thickness = value;
                OnPropertyChanged();
            }
        }
    }
}
