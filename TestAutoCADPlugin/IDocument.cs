﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin
{
    public interface IDocument
    {
        IEnumerable<ILayer> GetLayers();

        void SaveChanges();

        event EventHandler OnClose;
    }
}
