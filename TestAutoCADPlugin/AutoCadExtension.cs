﻿using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace TestAutoCADPlugin
{
    public class AutoCadExtension : IExtensionApplication
    {
        private static PaletteSet paletteSet = null;
        private static EditPrimitivesWindow control;

        public void Initialize()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        [CommandMethod("AUTOCAD")]
        public void AutoCadCommand()
        {
            if (paletteSet == null)
            {
                paletteSet = new PaletteSet("Primitives editor");
                
                paletteSet.DockEnabled = (DockSides)((int)DockSides.Left + (int)DockSides.Right);               

                if (control == null)
                {
                    IDocumentManager documentManager = new AutoCad();
                    control = new EditPrimitivesWindow(documentManager);
                }
                paletteSet.AddVisual("Editor window", control);
                
                control.LoadLayers();
            }
            paletteSet.KeepFocus = true;
            paletteSet.Visible = true;
            paletteSet.Size = new Size(600, 400);
            paletteSet.Dock = DockSides.Left;

            paletteSet.StateChanged += PaletteSet_StateChanged;
        }

        private void PaletteSet_StateChanged(object sender, PaletteSetStateEventArgs e)
        {
            if (e.NewState == StateEventIndex.Show)
            {
                control.LoadLayers();
            }
        }

        public void Terminate()
        {
        }

        // Fix AutoCAD not load dependency dlls from plugin folder
        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var asemblyLocation = Assembly.GetExecutingAssembly().Location;
            var pluginLocation = Path.GetDirectoryName(asemblyLocation);

            var notFoundAssemblyName = args.Name.Substring(0, args.Name.IndexOf(','));
            var filename = Path.Combine(pluginLocation, notFoundAssemblyName + ".dll");
            if (File.Exists(filename))
            {
                return Assembly.LoadFrom(filename);
            }
            return default(Assembly);
        }
    }
}
