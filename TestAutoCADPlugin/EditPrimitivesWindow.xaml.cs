﻿using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using System;
using System.Windows.Controls;

namespace TestAutoCADPlugin
{
    /// <summary>
    /// Interaction logic for EditPrimitivesWindow.xaml
    /// </summary>
    public partial class EditPrimitivesWindow : UserControl
    {
        public EditPrimitivesWindow(IDocumentManager documentManager)
        {
            if (documentManager == null)
            {
                throw new ArgumentNullException(nameof(documentManager));
            }
            InitializeComponent();

            Messenger.Default.Send(documentManager);     
        }

        public void LoadLayers()
        {
            Messenger.Default.Send(Message.LoadLayers);
        }
    }
}
