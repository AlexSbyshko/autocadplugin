﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin
{
    public interface ICircle : IPrimitive
    {
        IPosition CenterPosition { get; set; }

        double Radius { get; set; }
    }
}
