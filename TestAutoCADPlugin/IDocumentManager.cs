﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin
{
    public interface IDocumentManager
    {
        IDocument CurrentDocument { get; }

        event EventHandler CurrentDocumentChanged;
    }
}
