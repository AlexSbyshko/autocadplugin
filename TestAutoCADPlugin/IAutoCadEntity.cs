﻿using Autodesk.AutoCAD.DatabaseServices;

namespace TestAutoCADPlugin
{
    public interface IAutoCadEntity
    {
        ObjectId Id { get; }

        void AcceptChanges(Transaction transaction);
    }
}
