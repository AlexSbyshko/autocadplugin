﻿using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace TestAutoCADPlugin.Adapters
{
    public class CircleAdapter : PrimitiveAdapter, ICircle
    {
        private Circle autocadCircle;

        public CircleAdapter(Circle autocadCircle) 
            : base(autocadCircle)
        {
            this.autocadCircle = autocadCircle;
            var circleCenterPosition = autocadCircle.Center;
            CenterPosition = new PositionAdapter(circleCenterPosition);
            Radius = autocadCircle.Radius;
            Thickness = autocadCircle.Thickness;
        }

        public double Radius { get; set; }

        public IPosition CenterPosition { get; set; }

        public override void AcceptChanges(Transaction transaction)
        {
            var editedAutocadCircle = (Circle)transaction.GetObject(autocadCircle.Id, OpenMode.ForWrite);
            editedAutocadCircle.Center = new Point3d(CenterPosition.X, CenterPosition.Y, CenterPosition.Z);
            editedAutocadCircle.Radius = Radius;
            editedAutocadCircle.Thickness = Thickness;
        }
    }
}
