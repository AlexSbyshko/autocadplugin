﻿using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace TestAutoCADPlugin.Adapters
{
    public class PointAdapter : PrimitiveAdapter, IPoint
    {
        private DBPoint autocadPoint;

        public PointAdapter(DBPoint autocadPoint)
            : base(autocadPoint)
        {
            this.autocadPoint = autocadPoint;

            var pointPosition = autocadPoint.Position;
            Position = new PositionAdapter(pointPosition);
            Thickness = autocadPoint.Thickness;
        }

        public IPosition Position { get; set; }

        public override void AcceptChanges(Transaction transaction)
        {
            var editedAutocadPoint = (DBPoint)transaction.GetObject(autocadPoint.Id, OpenMode.ForWrite);
            editedAutocadPoint.Position = new Point3d(Position.X, Position.Y, Position.Z);
            editedAutocadPoint.Thickness = Thickness;
        }
    }
}
