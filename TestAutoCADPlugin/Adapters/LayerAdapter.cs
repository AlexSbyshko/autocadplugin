﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Autodesk.AutoCAD.DatabaseServices;

namespace TestAutoCADPlugin.Adapters
{
    public class LayerAdapter : ILayer, IAutoCadEntity
    {
        private LayerTableRecord autocadLayer;

        public LayerAdapter(LayerTableRecord autocadLayer)
        {
            this.autocadLayer = autocadLayer;

            Id = autocadLayer.Id;

            Name = autocadLayer.Name;

            var layerColor = autocadLayer.Color;
            var red = layerColor.Red;
            var green = layerColor.Green;
            var blue = layerColor.Blue;
            Color = Color.FromRgb(red, green, blue);

            IsVisible = !autocadLayer.IsOff;
        }

        public ObjectId Id { get; set; }

        public Color Color { get; set; }

        public string Name { get; set; }

        public bool IsVisible { get; set; }

        public bool IsZero { get; set; }

        public List<IPrimitive> Primitives { get; set; } = new List<IPrimitive>();

        public void AcceptChanges(Transaction transaction)
        {
            var editedAutocadLayer = (LayerTableRecord)transaction.GetObject(autocadLayer.Id, OpenMode.ForWrite);
            editedAutocadLayer.Name = Name;
            editedAutocadLayer.Color = Autodesk.AutoCAD.Colors.Color.FromRgb(Color.R, Color.G, Color.B);
            editedAutocadLayer.IsOff = !IsVisible;
        }
    }
}