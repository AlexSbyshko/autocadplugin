﻿using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin.Adapters
{
    public class PositionAdapter : IPosition
    {
        private Point3d position;

        public PositionAdapter(Point3d position)
        {
            this.position = position;

            X = position.X;
            Y = position.Y;
            Z = position.Z;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public double Z { get; set; }
    }
}
