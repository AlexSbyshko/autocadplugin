﻿using Autodesk.AutoCAD.DatabaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutoCADPlugin.Adapters
{
    public abstract class PrimitiveAdapter : IPrimitive, IAutoCadEntity
    {
        private Entity entity;

        public PrimitiveAdapter(Entity entity)
        {
            Id = entity.Id;
            LayerId = entity.LayerId;
        }

        public ObjectId LayerId { get; set; }

        public ObjectId Id { get; private set; }

        public ILayer Layer { get; set; }        

        public double Thickness { get; set; }

        public static PrimitiveAdapter ByEntity(Entity entity)
        {
            if (entity is Circle)
            {
                return new CircleAdapter((Circle)entity);
            }
            else if (entity is DBPoint)
            {
                return new PointAdapter((DBPoint)entity);
            }
            else if (entity is Line)
            {
                return new LineAdapter((Line)entity);
            }
            return null;
        }

        public abstract void AcceptChanges(Transaction transaction);
    }
}
