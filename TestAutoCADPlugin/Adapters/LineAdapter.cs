﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace TestAutoCADPlugin.Adapters
{
    public class LineAdapter : PrimitiveAdapter, ILine
    {
        private Line autocadLine;

        public LineAdapter(Line autocadLine)
            : base(autocadLine)
        {
            this.autocadLine = autocadLine;

            var lineStartPosition = autocadLine.StartPoint;
            var lineEndPosition = autocadLine.EndPoint;
            StartPosition = new PositionAdapter(lineStartPosition);
            EndPosition = new PositionAdapter(lineEndPosition);
            Thickness = autocadLine.Thickness;
        }

        public IPosition EndPosition { get; set; }

        public IPosition StartPosition { get; set; }

        public override void AcceptChanges(Transaction transaction)
        {
            var editedAutocadLine = (Line)transaction.GetObject(autocadLine.Id, OpenMode.ForWrite);
            editedAutocadLine.StartPoint = new Point3d(StartPosition.X, StartPosition.Y, StartPosition.Z);
            editedAutocadLine.EndPoint = new Point3d(EndPosition.X, EndPosition.Y, EndPosition.Z);
            editedAutocadLine.Thickness = Thickness;
        }
    }
}
