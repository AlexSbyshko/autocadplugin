﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Moq;
using TestAutoCADPlugin.ViewModels;
using System.Reflection;
using System.Collections.ObjectModel;

namespace TestAutoCADPlugin.Tests
{
    [TestFixture]
    public class MainViewModelTests
    {
        MainViewModel viewModel;
        Mock<IDocumentManager> documentManager;
        Mock<IDocument> documentMock;

        [SetUp]
        public void SetUp()
        {
            documentMock = new Mock<IDocument>();
            documentManager = new Mock<IDocumentManager>();
            documentManager.Setup(m => m.CurrentDocument).Returns(documentMock.Object);

            viewModel = new MainViewModel(documentManager.Object);            
        }

        [Test]
        public void Should_Return_Empty_List_If_DocumentManager_Is_Not_Set()
        {
            var viewModel = new MainViewModel(null);

            viewModel.LoadLayersCommand.Execute(new object());

            Assert.That(viewModel.Layers, Is.Empty);
        }

        [Test]
        public void Should_Return_Empty_List_If_CurrentDocument_Is_Not_Set()
        {
            documentManager.Setup(m => m.CurrentDocument).Returns((IDocument)null);

            viewModel.LoadLayersCommand.Execute(new object());

            Assert.That(viewModel.Layers, Is.Empty);
        }

        [Test]
        public void Should_Fill_Layers_From_Document()
        {
            var layer1 = Mock.Of<ILayer>(l => l.Name == "layer1");
            var layer2 = Mock.Of<ILayer>(l => l.Name == "layer2");
            var layers = new List<ILayer> { layer1, layer2 };
            documentMock.Setup(d => d.GetLayers()).Returns(layers);

            viewModel.LoadLayersCommand.Execute(new object());

            Assert.That(viewModel.Layers.Select(l => l.Name), Is.EquivalentTo(layers.Select(l => l.Name)));
        }

        [Test]
        public void Should_Clean_Layers_Before_Each_Load()
        {
            viewModel.Layers = new ObservableCollection<LayerViewModel>
            {
                new LayerViewModel(Mock.Of<ILayer>()),
                new LayerViewModel(Mock.Of<ILayer>())
            };
            documentMock.Setup(d => d.GetLayers()).Returns(Enumerable.Empty<ILayer>());

            viewModel.LoadLayersCommand.Execute(new object());

            Assert.That(viewModel.Layers, Is.Empty);
        }

        [Test]
        public void Should_Set_Primitives_To_Correct_Collections_Of_Layers()
        {
            var point = Mock.Of<IPoint>();
            var circle = Mock.Of<ICircle>();
            var line = Mock.Of<ILine>();
            var layer = Mock.Of<ILayer>(l => l.Primitives == new List<IPrimitive> { point, circle, line });
            documentMock.Setup(d => d.GetLayers()).Returns(new List<ILayer> { layer });

            viewModel.LoadLayersCommand.Execute(new object());

            var layerViewModel = viewModel.Layers.First();
            Assert.That(layerViewModel.Points.Count, Is.EqualTo(1));
            Assert.That(layerViewModel.Circles.Count, Is.EqualTo(1));
            Assert.That(layerViewModel.Lines.Count, Is.EqualTo(1));            
        }

        [Test]
        public void Should_Save_Changes_When_CurrentDocument_Is_Set()
        {
            viewModel.SaveChangesCommand.Execute(new object());

            documentMock.Verify(s => s.SaveChanges(), Times.Once);
        }

        [Test]
        public void Should_Do_Not_Save_Changes_When_CurrentDocument_Is_Not_Set()
        {
            documentManager.Setup(m => m.CurrentDocument).Returns((IDocument)null);
            viewModel = new MainViewModel(documentManager.Object);

            viewModel.SaveChangesCommand.Execute(new object());

            documentMock.Verify(s => s.SaveChanges(), Times.Never);
        }
    }
}
